import React from 'react';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from "@material-ui/core/CssBaseline";

const theme = createMuiTheme({
  palette: {
    background: {
      paper: '#fff',
      default: "#6ba2e0"
    },
  },
  overrides: {
    MuiPaper: {
      root: {
        padding: 20
      }
    },
  },
  props: {
    MuiPaper: {
      square: true,
    }
  }
});

type PropsType = {
  children: React.ReactNode;
};

export default function(props: PropsType) {
  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline/>
      { props.children }
    </MuiThemeProvider>
  );
}
