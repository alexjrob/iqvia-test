import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
// import { createBrowserHistory } from 'react-router';
import PageContacts from './components/PageContacts';

// const history = createBrowserHistory();

function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/contact/:contactId" component={PageContacts} />
        <Route path="/contacts" component={PageContacts} />
        <Route path="/" component={PageContacts} />
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
