import React, { Component } from "react";
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import Routes from './Routes';
import Theme from './Theme';

const apolloClient = new ApolloClient({
  uri: '/',
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={apolloClient}>
        <Theme>
          <Routes />
        </Theme>
      </ApolloProvider>
    );
  }
}

export default App;
