import React from 'react';
import { withRouter } from 'react-router-dom';
import { Grid, Paper, Button } from '@material-ui/core';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { Typography } from '@material-ui/core';
import ContactList from '../ContactList';
import ContactView from '../ContactView';

function PageContacts(props: any) {
  const { history } = props;
  const { contactId } = props.match.params;
  const listRequest = useQuery(gql`{
    contacts {
      id,
      name,
    }
  }`);

  return (
    <div style={{padding: 32}}>
      <Grid container direction="row" spacing={16}>
        <Grid item xs>
          <Paper>
            <Typography variant="h2">Contacts</Typography>
            <ContactList 
              loading={listRequest.loading}
              data={listRequest.data}
              error={listRequest.error}
            />
            <Button onClick={() => history.push('/contacts')}>
              + Add Contact
            </Button>
          </Paper>
        </Grid>
        <Grid item xs>
          <Paper>
              <ContactView contactId={contactId} refreshList={listRequest.refetch}/>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}

export default withRouter(PageContacts);
