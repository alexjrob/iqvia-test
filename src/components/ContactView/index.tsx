import React, {useState} from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { withRouter } from 'react-router';
import {
  Button,
  Typography,
  FormControl,
  Input,
  InputLabel,
  FormHelperText
} from '@material-ui/core';
import { Contact } from '../../types';

type ContactViewProps = {
  contactId?: string;
  refreshList: () => void;
};

function ContactView({contactId, refreshList}: ContactViewProps) {
  const {loading, error, data} = useQuery(gql`
    query GET_CONTACT($id: ID) {
      contact(id: $id) {
        id,
        name,
        email,
      }
    }
  `, {
    skip: !contactId,
    variables: { id: contactId },
  });

  if(!contactId) {
    return <Add refreshList={refreshList}/>;
  }

  if(loading) {
    return <div>Loading</div>;
  }

  if(error) {
    return <div>Error</div>;
  }

  const contact = data.contact;

  return (
    <Edit
      contact={contact}
      refreshList={refreshList}
    />
  );
}

type EditProps = {
  contact: Contact;
  refreshList: () => void;
};

function Edit({ contact, refreshList }: EditProps) {
    const [deleteContact] = useMutation(gql`
      mutation DELETE_CONTACT($id: ID) {
        deleteContact(id: $id)
      }
    `);
    const onDelete = async () => {
      await deleteContact({variables: {id: contact.id}});
      refreshList();
      // history.push('/contacts')
    };

    const [updateContact] = useMutation(gql`
      mutation EDIT_CONTACT($id: ID, $email: String!, $name: String!) {
        updateContact(contact: {id: $id, name: $name, email: $email}) {
          id
        }
      }
    `);


    const onSubmit: ContactFormOnSubmit = async  ({ name, email }) => {
      await updateContact({
        variables: {
          id: contact.id,
          name,
          email,
        }
      });
      refreshList();
    }

  return (
    <section>
      <header>
        <Typography inline variant="h2">Contact</Typography>
        {' '}
        <Button onClick={ onDelete }>
          Delete
        </Button>
        <Typography variant="caption">ID: { contact.id }</Typography>
      </header>
      <ContactForm
        key={contact.id}
        initialName={contact.name}
        initialEmail={contact.email}
        onSubmit={onSubmit}
      />
    </section>
  );
}

type AddProps = {
  refreshList: () => void;
};

// Wrap around to avoid `withRouter` prop types issues
function Add({refreshList}: AddProps) {
  const WithHistory = withRouter(
    function _Add({history}) {
      const [save] = useMutation(gql`
        mutation ADD_CONTACT($email: String!, $name: String!) {
          addContact(contact: {name: $name, email: $email}) {
            id
          }
        }
      `);

      const submit: ContactFormOnSubmit = async  ({ name, email }) => {
        const { data } = await save({ variables: { name, email }});
        refreshList();
        history.push('/contact/' + data.addContact.id)
      }
      return (
        <div>
          <Typography inline variant="h2">Add Contact</Typography>
          <ContactForm onSubmit={submit}/>
        </div>
      );
    }
  );
  return <WithHistory />
}

type ContactFormOnSubmit = ({name, email}: {name: string, email: string}) => any;

type ContactFormProps = {
  initialName?: string;
  initialEmail?: string;
  readOnly?: boolean;
  onSubmit?: ContactFormOnSubmit;
};

function ContactForm({initialName = '', initialEmail = '', onSubmit, readOnly = false}: ContactFormProps) {
  const [name, setName] = useState(initialName);
  const [email, setEmail] = useState(initialEmail);
  const nameError = !name && 'Name required';
  const emailError = !email.match(/.@./) && 'Enter a valid email address';
  const disableSubmit = !!(nameError || emailError);

  function submit() {
    if(onSubmit) {
      onSubmit({name, email});
    }
  }


  return (
    <div>
      <div>
        <FormControl fullWidth margin="dense">
          <InputLabel>
            Name
          </InputLabel>
          <Input
            readOnly={readOnly}
            type="text"
            value={name}
            onChange={ e => setName(e.currentTarget.value) }
          />
          <FormHelperText error>{ nameError }</FormHelperText>
        </FormControl>

        <FormControl fullWidth margin="dense">
          <InputLabel>
            Email
          </InputLabel>
          <Input
            readOnly={readOnly}
            type="text"
            value={email}
            onChange={ e => setEmail(e.currentTarget.value) }
          />
          <FormHelperText error>{ emailError }</FormHelperText>
        </FormControl>
      </div>
      { onSubmit && ( 
        <Button onClick={ submit } disabled={disableSubmit}>
          Save
        </Button>
      )}
    </div>
  );
}

export default ContactView;
