import React from 'react';
import { ListItem } from '@material-ui/core';
import { ContactStub } from '../../types';
import { Link } from 'react-router-dom';
type ContactListItemProps = {
  contactStub: ContactStub;
};

function ContactListItem({contactStub}: ContactListItemProps) {
  // return (
  //   <Link to={'/contact/' + contactStub.id}>
  //       <div>{ contactStub.name}</div>
  //   </Link>
  // );
  return (
    <Link to={'/contact/' + contactStub.id}>
      <ListItem>
        <div>{ contactStub.name}</div>
      </ListItem>
    </Link>
  );
}

export default ContactListItem;
