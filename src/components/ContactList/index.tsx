import React from 'react';
import { List } from '@material-ui/core';
import ContactListItem from '../ContactListItem';
import { ContactStub } from '../../types';

type ContactListProps = {
  loading: boolean,
  error: any,
  data: { contacts: ContactStub[] },
};

function ContactList({loading, error, data}: ContactListProps ) {

  if(loading) {
    return (
      <div>Loading</div>
    );
  }

  if(error) {
    return (
      <div>Error</div>
    );
  }

  const contactStubs: ContactStub[] = data.contacts;

  if(!contactStubs.length) {
    return <div><em>No contacts</em></div>
  }
  return (
    <List>
      {
        contactStubs.map( contactStub => {
          return <ContactListItem key={contactStub.id} contactStub={contactStub} />
        })
      }
    </List>
  );
}

export default ContactList;
