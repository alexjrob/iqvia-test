export type ContactStub = {
  id: string;
  name: string;
};

export type Contact = {
  id: string;
  name: string;
  email: string;
};
